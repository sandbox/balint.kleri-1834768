(function ($) {

/**
 * Functionality to display and interact with a map using Google Maps API.
 */

Drupal.geoCoordinates = Drupal.geoCoordinates || {};

/**
 * Attaches the Geo Coordinates behavior to each Geo Coordinates map element.
 */
Drupal.behaviors.geoCoordinates = {
  attach: function (context, settings) {
    var currentMapSettings;
    var mapOptions = {};
    var map;
    var marker;

    // Settings have been created by the theme function for each map.
    for (var i in Drupal.settings.GeoCoordinatesMaps) {
      currentMapSettings = Drupal.settings.GeoCoordinatesMaps[i];

      // Set the type of the map.
      mapOptions.mapTypeId = google.maps.MapTypeId.ROADMAP;

      // Set the default center of the map.
      // @todo Provide a good default position. This is Gothenburg currently.
      mapOptions.center = new google.maps.LatLng(57.7208, 11.9882);
      if (currentMapSettings.center != undefined) {
        mapOptions.center = new google.maps.LatLng(currentMapSettings.center.latitude, currentMapSettings.center.longitude);
      }

      // Set the default zooming level of the map.
      mapOptions.zoom = 10;
      if (currentMapSettings.zoom != undefined) {
        mapOptions.zoom = currentMapSettings.zoom;
      }

      // Initialize the map.
      map = new google.maps.Map($('.' + currentMapSettings.class + ' > div')[0], mapOptions);

      // Initialize the marker.
      marker = new google.maps.Marker({
        position: map.getCenter(),
        map: map,
        draggable: true
      });
      Drupal.geoCoordinates.attachDragEvents(marker, currentMapSettings.inputId);
    }
  }
};

/**
 * Registers event notifications for dragging.
 */
Drupal.geoCoordinates.attachDragEvents = function(marker, inputId) {
  google.maps.event.addListener(marker, 'drag', function() {
    Drupal.geoCoordinates.updateCoordinates(inputId, marker.getPosition());
  });
  google.maps.event.addListener(marker, 'dragend', function() {
    Drupal.geoCoordinates.updateCoordinates(inputId, marker.getPosition());
  });
};

/**
 * Extracts coordinates from the marker position. Updates the textfield.
 */
Drupal.geoCoordinates.updateCoordinates = function(inputId, position) {
  $('#' + inputId).val(position.lat() + ', ' + position.lng());
};

})(jQuery);
